﻿using EducationGroupsMVC.Data;
using EducationGroupsMVC.Models;
using EducationGroupsMVC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EducationGroupsMVC.Controllers
{
    public class CourseController : Controller
    {
        private readonly ApplicationDbContext _context;
        public CourseController(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Create()
        {
            var teachers = await _context.Teachers.ToListAsync();
            var createCourseViewModel = new CreateCourseViewModel();
            foreach (var teacher in teachers)
            {
                createCourseViewModel.TeachersSelect.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = teacher.FullName,
                    Value = teacher.Id.ToString()
                });
            }
            return View(createCourseViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateCourseViewModel createCourseViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(createCourseViewModel);
            }
            var course = new Course
            {
                Name = createCourseViewModel.Name,
                Description = createCourseViewModel.Description,
                TeacherId = createCourseViewModel.TeacherId,
            };
            await _context.Courses.AddAsync(course);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
        public async Task<IActionResult> Edit(int id)
        {
            var course = await _context.Courses.FindAsync(id);
            if (course is null)
            {
                return RedirectToAction("Index", "Home");
            }
            var editCourseViewModel = new EditCourseViewModel
            {
                Name = course.Name,
                Description = course.Description,
                TeacherId = course.TeacherId
            };
            var teachers = await _context.Teachers.ToListAsync();
            foreach (var teacher in teachers)
            {
                editCourseViewModel.TeachersSelect.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = teacher.FullName,
                    Value = teacher.Id.ToString(),
                    Selected = course.TeacherId == teacher.Id
                });
            }
            return View(editCourseViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int id, EditCourseViewModel editCourseViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(editCourseViewModel);
            }
            var course = await _context.Courses.AsNoTracking().FirstOrDefaultAsync(s => s.Id == id);
            if (course is null)
            {
                return RedirectToAction("Index", "Home");
            }
            var newCourse = new Course
            {
                Id = id,
                Name= editCourseViewModel.Name,
                Description= editCourseViewModel.Description,
                TeacherId = editCourseViewModel.TeacherId
            };
            _context.Courses.Update(newCourse);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home");
            }
            var course = await _context.Courses.FindAsync(id);
            if (course is null)
            {
                return RedirectToAction("Index", "Home");
            }
            _context.Courses.Remove(course);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}

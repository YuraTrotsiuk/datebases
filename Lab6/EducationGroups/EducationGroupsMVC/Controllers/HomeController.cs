﻿using EducationGroupsMVC.Data;
using EducationGroupsMVC.Models;
using EducationGroupsMVC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace EducationGroupsMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var courses = await _context.Courses
                .Include(c => c.Teacher)
                .Include(c => c.Groups)
                .ThenInclude(g => g.Faculty)
                .ToListAsync();
            var students = _context.Students
                .Include(s => s.Group)
                .ThenInclude(g => g.Faculty);
            IndexHomeViewModel indexHomeViewModel = new()
            {
                Courses = courses,
                Students = students
            };
            return View(indexHomeViewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
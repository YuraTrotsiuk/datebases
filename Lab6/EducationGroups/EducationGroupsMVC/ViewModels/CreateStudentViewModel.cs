﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace EducationGroupsMVC.ViewModels
{
    public class CreateStudentViewModel
    {
        [Required(ErrorMessage = "Full name is required")]
        public string FullName { get; set; } = null!;
        [Range(16, 100, ErrorMessage = "Name must be in range [16, 100]")]
        [Required(ErrorMessage = "Age name is required")]
        public int Age { get; set; }
        [Required(ErrorMessage = "Group is required")]
        public int GroupId { get; set; }
        public ICollection<SelectListItem> GroupsSelect { get; set; } = new List<SelectListItem>();

    }
}

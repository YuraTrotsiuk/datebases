﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace EducationGroupsMVC.ViewModels
{
    public class EditCourseViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; } = null!;
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; } = null!;
        [Required(ErrorMessage = "Teacher is required")]
        public int TeacherId { get; set; }
        public ICollection<SelectListItem> TeachersSelect { get; set; } = new List<SelectListItem>();
    }
}

﻿using EducationGroupsMVC.Models;

namespace EducationGroupsMVC.ViewModels
{
    public class IndexHomeViewModel
    {
        public IEnumerable<Student> Students { get; set; } = null!;
        public IEnumerable<Course> Courses { get; set; } = null!;
    }
}

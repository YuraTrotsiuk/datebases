﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EducationGroupsMVC.Models
{
    public class Teacher
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; } = null!;
        [InverseProperty("Teacher")]
        public ICollection<Course> Courses { get; set; } = new List<Course>();
    }
}

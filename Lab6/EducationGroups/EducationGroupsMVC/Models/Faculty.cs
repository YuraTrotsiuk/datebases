﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace EducationGroupsMVC.Models
{
    public class Faculty
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        [InverseProperty("Faculty")]
        public ICollection<Group> Groups { get; set; } = new List<Group>();
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EducationGroupsMVC.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; } = null!;
        public int Age { get; set; }
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        [InverseProperty("Students")]
        public Group Group { get; set; } = null!;
    }
}

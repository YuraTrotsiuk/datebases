﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EducationGroupsMVC.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        [ForeignKey("Teacher")]
        public int TeacherId { get; set; }
        [InverseProperty("Course")]
        public ICollection<Group> Groups { get; set; } = new List<Group>();
        [InverseProperty("Courses")]
        public Teacher Teacher { get; set; } = null!;
    }
}

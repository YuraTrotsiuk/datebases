﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace EducationGroupsMVC.Models
{
    public class Group
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        [ForeignKey("Faculty")]
        public int FacultyId { get; set; }
        [InverseProperty("Groups")]
        public Faculty Faculty { get; set; } = null!;
        [InverseProperty("Group")]
        public ICollection<Student> Students { get; set; } = new List<Student>();
        [ForeignKey("CourseController")]
        public int CourseId { get; set; }
        [InverseProperty("Groups")]
        public Course Course { get; set; } = null!;
    }
}

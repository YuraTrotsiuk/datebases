﻿using EducationGroupsMVC.Models;

namespace EducationGroupsMVC.Data
{
    public static class Seed
    {
        public static IApplicationBuilder SeedData(this IApplicationBuilder applicationBuilder)
        {
            using (var scope = applicationBuilder.ApplicationServices.CreateScope())
            {
                using (var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>())
                {
                    try
                    {
                        if (!context.Teachers.Any())
                        {
                            context.Teachers.AddRange(new List<Teacher>
                            {
                                new()
                                {
                                    FullName = "Ivanenko Ivan Ivanovych"
                                },
                            });
                        }
                        context.SaveChanges();
                        if (!context.Faculties.Any())
                        {
                            context.Faculties.AddRange(new List<Faculty>
                            {
                                new()
                                {
                                    Name = "FICT",
                                },
                            });
                        }
                        context.SaveChanges();
                        if (!context.Courses.Any())
                        {
                            context.Courses.AddRange(new List<Course>
                            {
                                new()
                                {
                                    TeacherId = 1,
                                    Name = "CourseController #1",
                                    Description = "Some description for course"
                                },
                            });
                        }
                        context.SaveChanges();
                        if (!context.Groups.Any())
                        {
                            context.Groups.AddRange(new List<Group>
                            {
                                new()
                                {
                                    Name = "IPZ-21-1",
                                    FacultyId = 1,
                                    CourseId = 1,
                                },
                                new()
                                {
                                    Name = "IPZ-21-2",
                                    FacultyId = 1,
                                    CourseId = 1,
                                },
                            });
                        }
                        context.SaveChanges();
                        if (!context.Students.Any())
                        {
                            context.Students.AddRange(new List<Student>
                            {
                                new()
                                {
                                    FullName = "Peternko Petro Petrovych",
                                    Age = 17,
                                    GroupId = 1
                                },
                                new()
                                {
                                    FullName = "Symonenko Symon Semenovych",
                                    Age = 18,
                                    GroupId = 1
                                },
                                new()
                                {
                                    FullName = "Pavlenko Pavlo Pavlovych", 
                                    Age = 18,
                                    GroupId = 2
                                },
                            });
                        }
                        context.SaveChanges();
                    }
                    catch
                    {
                        return applicationBuilder;
                    }
                }
                return applicationBuilder;
            }
        }
    }
}

﻿------------1------------
DROP DATABASE IF EXISTS groups
CREATE DATABASE groups

USE groups
DROP TABLE IF EXISTS [SelectedCourse];
DROP TABLE IF EXISTS [Student];
DROP TABLE IF EXISTS [Group];
DROP TABLE IF EXISTS [Faculty];
DROP TABLE IF EXISTS [Course];
DROP TABLE IF EXISTS [Teacher];

CREATE TABLE [Faculty](
[IDFaculty] INT IDENTITY(1,1) PRIMARY KEY,
[Name] NVARCHAR(50) NOT NULL,
[Description] NVARCHAR(255) NOT NULL
);

CREATE TABLE [Group](
[IDGroup] INT IDENTITY(1,1) PRIMARY KEY,
[Name] NVARCHAR(50) NOT NULL,
[IDFaculty] INT REFERENCES [Faculty] ([IDFaculty])
);

CREATE TABLE [Student](
[IDStudent] INT IDENTITY(1,1) PRIMARY KEY,
[Name] NVARCHAR(50) NOT NULL,
[Surname] NVARCHAR(50) NOT NULL,
[IDGroup] INT NOT NULL REFERENCES [Group] ([IDGroup])
);

CREATE TABLE [Teacher](
[IDTeacher] INT IDENTITY(1,1) PRIMARY KEY,
[Name] NVARCHAR(50) NOT NULL,
[Surname] NVARCHAR(50) NOT NULL,
[Status] CHAR(10) NOT NULL
);

CREATE TABLE [Course](
[IDCourse] INT IDENTITY(1,1) PRIMARY KEY,
[Name] NVARCHAR(50) NOT NULL,
[Description] NVARCHAR(255) NOT NULL,
[Price] FLOAT NOT NULL,
[IDTeacher] INT NOT NULL REFERENCES [Teacher] ([IDTeacher]),
[Status] CHAR(10),
);

CREATE TABLE [SelectedCourse](
[IDSelectedCourse] INT IDENTITY(1,1) PRIMARY KEY,
[IDStudent] INT NOT NULL REFERENCES [Student] ([IDStudent]),
[IDCourse] INT NOT NULL REFERENCES [Course] ([IDCourse]),
[Status] CHAR(10) NOT NULL
)

------------2------------
INSERT INTO [Faculty] ([Name], [Description]) VALUES 
(N'Фікт', N'Щось там з компами'),
(N'Фоа', N'Щось там з о і а'),
(N'Фзто', N'Тут опис')

INSERT INTO [Group] ([Name], [IDFaculty]) VALUES 
(N'РО-22', 2),
(N'ЛЗ-22', 1),
(N'ЩК-22', 3)

INSERT INTO [Student] ([Name], [Surname], [IDGroup]) VALUES
(N'Мікі', N'Маус', 1),
(N'Ерен', N'Йегер', 3),
(N'Марко', N'Діаз', 3),
(N'Ягамі', N'Лайт', 1)

INSERT INTO [Teacher] ([Name], [Surname], [Status]) VALUES
(N'Шевченко', N'Тарас', 'free'),
(N'Рой', N'Мустанг', 'free'),
(N'Чжун', N'Лі', 'free'),
(N'Дамблдор', N'Альбус', 'free')

INSERT INTO [Course] ([Name], [Description], [Price], [IDTeacher], [Status]) VALUES
(N'Комп', N'Мишка клавіатура монітор', 456, 3, 'active'),
(N'Бази даних', N'Впорядкування інформації', 700, 2, 'active'),
(N'Бек-енд', N'Пхп', 890, 2, 'active'),
(N'Фронт-енд', N'Джс', 370, 1, 'active'),
(N'Пайтон', N'Пайтон', 290, 1, 'active'),
(N'АСП.НЕТ', N'АСП.НЕТ', 460, 3, 'active')

INSERT INTO [SelectedCourse] ([IDStudent], [IDCourse], [Status]) VALUES
(1, 2, 'main'), (1, 3, 'main'), (1, 1, 'main'), (1, 5, 'main'), (1, 4, 'add'), (1, 6, 'add'),
(3, 6, 'add'), (3, 2, 'add'), (3, 5, 'main'), (3, 1, 'main'), (3, 3, 'main'), (3, 4, 'main')

------------3------------
	--	список курсів
SELECT * FROM Course

	--	список груп що сформовані для курсу
SELECT Course.[Name], COUNT(SelectedCourse.IDStudent) AS [Number of students] FROM Course
JOIN SelectedCourse on SelectedCourse.IDCourse = Course.IDCourse
WHERE SelectedCourse.[Status] = 'main'
GROUP BY Course.[Name]
HAVING COUNT(SelectedCourse.IDStudent) > 4 AND COUNT(SelectedCourse.IDStudent) < 21

	--	список зайнятих викладачів
SELECT Teacher.IDTeacher, Teacher.[Name], Surname FROM Teacher
FULL JOIN Course ON Course.IDTeacher = Teacher.IDTeacher
WHERE Course.IDTeacher IS NOT NULL 
GROUP BY Teacher.IDTeacher, Surname, Teacher.[Name]

	--	список відмінених курсів
SELECT Course.[Name], COUNT(SelectedCourse.IDStudent) AS [Number of students] FROM Course
JOIN SelectedCourse on SelectedCourse.IDCourse = Course.IDCourse
WHERE SelectedCourse.[Status] = 'main'
GROUP BY Course.[Name]
HAVING COUNT(SelectedCourse.IDStudent) < 5

------------4------------
	--процедуру, що визначає кількість рядків в таблицях БД і заносить результат в нову таблицю. 
DROP PROC IF EXISTS WriteRowNumber
GO
CREATE PROC WriteRowNumber AS
BEGIN
	DECLARE @RowNumber TABLE ([TableName] VARCHAR(255), [Number] INT)
	INSERT INTO @RowNumber ([TableName], [Number]) 
	EXEC sp_MSforeachtable 'SELECT ''?'' [TableName],  COUNT(*) [Number] FROM ?'
	SELECT * FROM @RowNumber
	DROP TABLE IF EXISTS RowNumber
	SELECT * INTO RowNumber FROM @RowNumber
END
GO
EXEC WriteRowNumber

	--процедуру, що визначає кількість полів в таблицях БД і заносить результат в нову таблицю.
DROP PROC IF EXISTS WriteColumnNumber
GO
CREATE PROC WriteColumnNumber AS
BEGIN
	SELECT TABLE_NAME AS [TableName], COUNT(COLUMN_NAME) AS [Number] INTO [ColumnNumber]
	FROM INFORMATION_SCHEMA.COLUMNS
	GROUP BY TABLE_NAME
	SELECT * FROM ColumnNumber
ENDGO
EXEC WriteColumnNumber

	--процедуру, що визначає для кожного поля таблиці, кількість значень, що не повторюються.
DROP PROC IF EXISTS UniqueValues
GO
CREATE PROC UniqueValues (@tableName VARCHAR(100)) AS
BEGIN
	DECLARE @sql VARCHAR(255);
	SET @sql = 'SELECT COLUMN_NAME, COUNT(DISTINCT ' + @tableName + '.' + COLUMN_NAME + ') AS UniqueValues ' +
	'FROM ' + @tableName + ' JOIN ' + INFORMATION_SCHEMA.COLUMNS + ' ON ' + @tableName + 
	'.object_id = COLUMNS.object_id ' + 'WHERE TABLE_NAME = ''' + @tableName + ''' ' +
	'GROUP BY COLUMN_NAME'
	EXEC @sql
END
GO
EXEC UniqueValues 'SelectedCourses'

------------5------------
	--a.	Для операції оновлення таблиці:
		--Тригер, що оновлює одночасно дані в двох таблицях
DROP TRIGGER IF EXISTS updating1
GO
CREATE TRIGGER updating1 ON Course
INSTEAD OF UPDATE AS
DECLARE @course int = (SELECT IDCourse FROM INSERTED), 
@teacher int = (SELECT IDTeacher FROM INSERTED)
BEGIN
	IF (UPDATE([IDTeacher]))
	BEGIN
		UPDATE Course SET IDTeacher = @teacher
		WHERE IDCourse = @course
		UPDATE Teacher SET [Status] = 'busy'
		WHERE IDTeacher = @teacher
	END
END

SELECT * FROM Teacher
SELECT * FROM Course

UPDATE Course SET [IDTeacher] = 1 WHERE IDCourse = 1
SELECT * FROM Teacher
SELECT * FROM Course

		--Тригер, що при оновленні відхиляє зміни, якщо є зв’язані дані в іншій таблиці
DROP TRIGGER IF EXISTS updating2
GO
CREATE TRIGGER updating2 ON Student INSTEAD OF UPDATE AS
DECLARE @id INT = (SELECT IDStudent FROM INSERTED)
DECLARE @name NVARCHAR(50) = (SELECT [Name] FROM INSERTED)
DECLARE @surname NVARCHAR(50) = (SELECT Surname FROM INSERTED)
DECLARE @group INT = (SELECT IDGroup FROM INSERTED)
BEGIN	
	IF EXISTS(
		SELECT IDStudent FROM SelectedCourse
		WHERE IDStudent = @id
	)
	BEGIN
		RAISERROR('Cannot update information about this student as they are choosing their courses', 16, 1)
	END
	ELSE
	BEGIN
		UPDATE Student SET [Name] = @name, [Surname] = @surname,
		[IDGroup] = @group WHERE IDStudent = @id
	END
END

SELECT * FROM Student
SELECT * FROM SelectedCourse

UPDATE Student SET [Name] = N'Міні' WHERE IDStudent = 1

	--b.	Для операції знищення даних з таблиці:
		--Тригер, що знищує зв’язані дані одночасно в двох таблицях
DROP TRIGGER IF EXISTS deleting1
GO
CREATE TRIGGER deleting1 ON Student INSTEAD OF DELETE AS
DECLARE @student INT = (SELECT IDStudent FROM DELETED)
BEGIN	
	IF EXISTS(
	SELECT IDSelectedCourse FROM SelectedCourse
	WHERE IDStudent = @student
	)
	BEGIN
		DELETE TOP(6) FROM SelectedCourse
		WHERE IDStudent = @student
	END
		
	IF NOT EXISTS(
	SELECT IDSelectedCourse FROM SelectedCourse
	WHERE IDStudent = @student
	)
	BEGIN
		DELETE FROM Student
		WHERE IDStudent = @student
	END
END

SELECT * FROM Student
SELECT * FROM SelectedCourse

DELETE Student WHERE IDStudent = 1
SELECT * FROM Student
SELECT * FROM SelectedCourse

		--Тригер, що при знищенні перевіряє наявність в іншій таблиці даних, що відповідають заданій умові і відхиляє знищення даних
DROP TRIGGER IF EXISTS deleting2
GO
CREATE TRIGGER deleting2 ON Faculty INSTEAD OF DELETE AS
DECLARE @id INT = (SELECT IDFaculty FROM DELETED)
BEGIN	
	IF EXISTS(
	SELECT IDFaculty FROM [Group]
	WHERE IDFaculty = @id
	)
	BEGIN
		RAISERROR('Cannot delete this faculty as it uses in some group information', 16, 1)
	END
	ELSE
	BEGIN
		DELETE FROM Faculty
		WHERE IDFaculty = @id
	END
END
SELECT * FROM Faculty
SELECT * FROM [Group]

DELETE Faculty WHERE IDFaculty = 2

	--c.	Для операції вставки даних:
		--Створити тригер, що реалізує вставку даних і зміну кількості рядків в таблиці (дані про кількість рядків в таблицях містяться в окремій таблиці).
DROP TRIGGER IF EXISTS inserting
GO
CREATE TRIGGER inserting ON SelectedCourse AFTER INSERT AS
BEGIN
	UPDATE RowNumber SET [Number] = (SELECT COUNT(*) FROM SelectedCourse)
	WHERE [TableName] = '[dbo].[SelectedCourse]'
END

SELECT * FROM SelectedCourse
SELECT * FROM RowNumber

INSERT INTO [SelectedCourse] ([IDStudent], [IDCourse], [Status]) VALUES
(2, 2, 'main'), (2, 3, 'main'), (2, 1, 'main'), (2, 5, 'main'), (2, 4, 'add'), (2, 6, 'add')
SELECT * FROM SelectedCourse
SELECT * FROM RowNumber

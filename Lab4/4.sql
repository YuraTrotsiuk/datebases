﻿--створення тестової бд
DROP DATABASE IF EXISTS lab4
CREATE DATABASE lab4

GO
USE lab4
DROP TABLE IF EXISTS [user];
DROP TABLE IF EXISTS [product];
DROP TABLE IF EXISTS [category];

CREATE TABLE [user](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[name] NVARCHAR(255) NOT NULL,
[email] NVARCHAR(50) NOT NULL,
[password] NVARCHAR(50) NOT NULL,
[role] NVARCHAR(50) NOT NULL,
);

CREATE TABLE [category](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[name] NVARCHAR(255) NOT NULL,
);

CREATE TABLE [product](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[name] NVARCHAR(50) NOT NULL,
[img] NVARCHAR(50) NOT NULL,
[number] INT NOT NULL,
[category_id] INT REFERENCES [category] ([id]),
);

--наповнення таблиць
GO
INSERT INTO [user] ([name], [email], [password], [role]) VALUES
(N'John Duo', N'aaa@mail.com', N'12345', N'user'),
(N'User 2', N'sss@mail.com', N'pass', N'user'),
(N'Dua Lip', N'nnn@mail.com', N'123', N'user')

INSERT INTO [category] ([name]) VALUES
(N'Food'), (N'Office')

INSERT INTO [product] ([name], [img], [number], [category_id]) VALUES
(N'PictureBook', 'sketchbook.png', 23, 2),
(N'Donut', 'donut.png', 5, 1),
(N'Pencil', 'pencil.png', 100, 2)

--процедури для перегляду
GO
DROP PROC IF EXISTS get_user_by_name
GO
CREATE PROC get_user_by_name (@name NVARCHAR(255)) AS
BEGIN
	SELECT * FROM [user] WHERE [name] = @name
END

GO
EXEC get_user_by_name N'Admin'

GO
DROP PROC IF EXISTS get_product_by_category_name
GO
CREATE PROC get_product_by_category_name (@name NVARCHAR(255)) AS
BEGIN
	SELECT [product].[name], [product].[img], [product].[number], [category].[name] FROM [product] 
	INNER JOIN [category] ON [product].[category_id] = [category].[id]
	WHERE [category].[name] = @name
END

GO
EXEC get_product_by_category_name N'Food'

GO
DROP PROC IF EXISTS get_product_by_number_less
GO
CREATE PROC get_product_by_number_less (@number INT) AS
BEGIN
	SELECT [product].[name], [product].[img], [product].[number], [category].[name] FROM [product] 
	INNER JOIN [category] ON [product].[category_id] = [category].[id]
	WHERE [number] <= @number
END

GO
EXEC get_product_by_number_less 23

--процедури на зміну
GO
DROP PROC IF EXISTS insert_user
GO
CREATE PROC insert_user (@name NVARCHAR(255), @email NVARCHAR(50), @password NVARCHAR(50), @role NVARCHAR(50)) AS
BEGIN
	INSERT INTO [user] ([name], [email], [password], [role]) VALUES
	(@name, @email, @password, @role)
END

GO
DROP PROC IF EXISTS update_user
GO
CREATE PROC update_user (@id INT, @name NVARCHAR(255), @email NVARCHAR(50), @password NVARCHAR(50), @role NVARCHAR(50)) AS
BEGIN
	UPDATE [user] SET [name] = @name, [email] = @email, [password] = @password, [role] = @role WHERE [id] = @id
END

GO
DROP PROC IF EXISTS delete_user
GO
CREATE PROC delete_user (@id INT) AS
BEGIN
	DELETE FROM [user] WHERE [id] = @id
END

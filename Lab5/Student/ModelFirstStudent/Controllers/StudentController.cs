﻿using ModelFirstStudent.Data;
using ModelFirstStudent.Models;
using ModelFirstStudent.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ModelFirstStudent.Controllers
{
    public class StudentController : Controller
    {
        private readonly ModelFirstStudentContext _context;

        public StudentController(ModelFirstStudentContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Create()
        {
            var groups = await _context.Groups.ToListAsync();
            var createStudentViewModel = new CreateStudentViewModel();
            foreach (var group in groups)
            {
                createStudentViewModel.GroupsSelect.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = group.Name,
                    Value = group.Id.ToString()
                });
            }
            return View(createStudentViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateStudentViewModel createStudentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(createStudentViewModel);
            }
            var student = new Student
            {
                Name = createStudentViewModel.Name,
                LastName = createStudentViewModel.LastName,
                GroupId = createStudentViewModel.GroupId,
            };
            await _context.Students.AddAsync(student);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Edit(int id)
        {
            var student = await _context.Students.FirstOrDefaultAsync(s => s.Id == id);
            if (student is null)
            {
                return RedirectToAction("Index", "Home");
            }
            var editStudentViewModel = new EditStudentViewModel
            {
                Name = student.Name,
                LastName = student.LastName,
                GroupId = student.GroupId,
            };
            var groups = await _context.Groups.ToListAsync();
            foreach (var group in groups)
            {
                editStudentViewModel.GroupsSelect.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = group.Name,
                    Value = group.Id.ToString(),
                    Selected = student.GroupId == group.Id
                });
            }
            return View(editStudentViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, EditStudentViewModel editStudentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(editStudentViewModel);
            }
            var student = await _context.Students.AsNoTracking().FirstOrDefaultAsync(s => s.Id == id);
            if (student is null)
            {
                return RedirectToAction("Index", "Home");
            }
            var newStudent = new Student
            {
                Id = id,
                Name = editStudentViewModel.Name,
                LastName = editStudentViewModel.LastName,
                GroupId = editStudentViewModel.GroupId,
            };
            _context.Students.Update(newStudent);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteStudentViewModel deleteStudentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home");
            }
            var student = await _context.Students.FirstOrDefaultAsync(s => s.Id == deleteStudentViewModel.Id);
            if (student is null)
            {
                return RedirectToAction("Index", "Home");
            }
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}

﻿using ModelFirstStudent.Data;
using ModelFirstStudent.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using ModelFirstStudent.ViewModels;

namespace ModelFirstStudent.Controllers
{
    public class HomeController : Controller
    {
        private readonly ModelFirstStudentContext _context;
        public HomeController(ModelFirstStudentContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var students = await _context.Students.Include(s => s.Group).ThenInclude(g => g.Faculty).ToListAsync();
            var indexHomeViewModel = new IndexHomeViewModel
            {
                Students = students
            };
            return View(indexHomeViewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace ModelFirstStudent.ViewModels
{
    public class DeleteStudentViewModel
    {
        [Required]
        public int Id { get; set; }
    }
}

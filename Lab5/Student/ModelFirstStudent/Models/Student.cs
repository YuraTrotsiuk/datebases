﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelFirstStudent.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string LastName { get; set; } = null!;
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        [InverseProperty("Students")]
        public Group Group { get; set; } = null!;
    }
}

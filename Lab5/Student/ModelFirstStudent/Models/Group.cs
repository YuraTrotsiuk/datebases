﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelFirstStudent.Models
{
    public class Group
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        [ForeignKey("Faculty")]
        public int FacultyId { get; set; }
        [InverseProperty("Groups")]
        public Faculty Faculty { get; set; } = null!;
        [InverseProperty("Group")]
        public ICollection<Student> Students { get; set; } = new List<Student>();
    }
}

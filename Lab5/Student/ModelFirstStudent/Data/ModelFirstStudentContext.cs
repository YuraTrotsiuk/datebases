﻿using Microsoft.EntityFrameworkCore;
using ModelFirstStudent.Models;

namespace ModelFirstStudent.Data
{
    public class ModelFirstStudentContext : DbContext
    {
        public ModelFirstStudentContext(DbContextOptions<ModelFirstStudentContext> options) : base(options) { }
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
    }
}

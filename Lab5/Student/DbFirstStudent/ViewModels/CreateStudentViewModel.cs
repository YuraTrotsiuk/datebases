﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace DbFirstStudent.ViewModels
{
    public class CreateStudentViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; } = null!;
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; } = null!;
        [Required(ErrorMessage = "Group is required")]
        public int GroupId { get; set; }
        public ICollection<SelectListItem> GroupsSelect { get; set; } = new List<SelectListItem>();
    }
}

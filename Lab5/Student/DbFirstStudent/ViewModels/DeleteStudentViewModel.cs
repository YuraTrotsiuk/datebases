﻿using System.ComponentModel.DataAnnotations;

namespace DbFirstStudent.ViewModels
{
    public class DeleteStudentViewModel
    {
        [Required]
        public int Id { get; set; }
    }
}

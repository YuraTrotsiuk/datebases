﻿using DbFirstStudent.Models;

namespace DbFirstStudent.ViewModels
{
    public class IndexHomeViewModel
    {
        public List<Student> Students { get; set; } = null!;
        public DeleteStudentViewModel DeleteStudentViewModel { get; set; } = null!;
    }
}

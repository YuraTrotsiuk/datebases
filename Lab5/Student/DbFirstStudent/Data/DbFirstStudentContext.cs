﻿using DbFirstStudent.Models;
using Microsoft.EntityFrameworkCore;

namespace DbFirstStudent.Data;

public partial class DbFirstStudentContext : DbContext
{
    public DbFirstStudentContext(DbContextOptions<DbFirstStudentContext> options) : base(options) { }

    public DbSet<Faculty> Faculties { get; set; }

    public DbSet<Group> Groups { get; set; }

    public DbSet<Student> Students { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Group>(entity =>
        {
            entity.HasOne(d => d.Faculty).WithMany(p => p.Groups)
                .HasForeignKey(d => d.FacultyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Groups_Faculties");
        });

        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasOne(d => d.Group).WithMany(p => p.Students)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Students_Groups");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

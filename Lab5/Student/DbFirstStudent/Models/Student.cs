﻿namespace DbFirstStudent.Models;

public partial class Student
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public int GroupId { get; set; }

    public Group Group { get; set; } = null!;
}

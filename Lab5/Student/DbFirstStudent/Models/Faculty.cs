﻿namespace DbFirstStudent.Models;

public partial class Faculty
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string DeanName { get; set; } = null!;

    public ICollection<Group> Groups { get; set; } = new List<Group>();
}

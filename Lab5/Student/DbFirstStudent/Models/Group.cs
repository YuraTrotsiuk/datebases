﻿namespace DbFirstStudent.Models;

public partial class Group
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int FacultyId { get; set; }

    public Faculty Faculty { get; set; } = null!;

    public ICollection<Student> Students { get; set; } = new List<Student>();
}

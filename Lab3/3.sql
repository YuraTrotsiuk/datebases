﻿-- test database
DROP DATABASE IF EXISTS gallery
CREATE DATABASE gallery

USE gallery
DROP TABLE IF EXISTS [Picture];
DROP TABLE IF EXISTS [User];
DROP TABLE IF EXISTS [Statistic_picture];
DROP TABLE IF EXISTS [Comment];
DROP TABLE IF EXISTS [Liked];
DROP TABLE IF EXISTS [Bought];
DROP TABLE IF EXISTS [Category];
DROP TABLE IF EXISTS [Tag];

CREATE TABLE [User](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[email] NVARCHAR(50) NOT NULL,
[password] NVARCHAR(255) NOT NULL,
[name] NVARCHAR(255) NOT NULL,
[role] NVARCHAR(255) NOT NULL,
);

CREATE TABLE [Category](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[name] NVARCHAR(255) NOT NULL,
);

CREATE TABLE [Tag](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[name] NVARCHAR(255) NOT NULL,
);

CREATE TABLE [Picture](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[name] NVARCHAR(50) NOT NULL,
[img] NVARCHAR(50) NOT NULL,
[tags] NVARCHAR(255) NOT NULL,
[artist_id] INT REFERENCES [User] ([id]),
[category_id] INT REFERENCES [Category] ([id]),
);

CREATE TABLE [Comment](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[user_id] INT REFERENCES [User] ([id]),
[picture_id] INT REFERENCES [Picture] ([id]),
[text] NVARCHAR(255) NOT NULL,
);

CREATE TABLE [Liked](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[user_id] INT REFERENCES [User] ([id]),
[picture_id] INT REFERENCES [Picture] ([id]),
);

CREATE TABLE [Bought](
[id] INT IDENTITY(1,1) PRIMARY KEY,
[user_id] INT REFERENCES [User] ([id]),
[picture_id] INT REFERENCES [Picture] ([id]),
);

--створити користувача з правами на модифікацію схеми даних та виконання операцій вставки та перегляду таблиць.

create login adminlogin with password = '1111'
use gallery
create user adminlogin for login adminlogin

--ролі БД
alter role db_backupoperator add member adminlogin
exec sp_helprolemember

--нова роль БД
create role administrator authorization db_ddladmin
alter role db_ddladmin add member adminlogin

--дозволи для користувача
grant select to adminlogin
grant insert to adminlogin
deny delete to adminlogin


SELECT DB_NAME() AS 'Database', p.name, p.type_desc, p.is_fixed_role, dbp.state_desc,
dbp.permission_name, so.name, so.type_desc
FROM sys.database_permissions dbp
LEFT JOIN sys.objects so ON dbp.major_id = so.object_id
LEFT JOIN sys.database_principals p ON dbp.grantee_principal_id = p.principal_id
--WHERE p.name = 'ProdDataEntry'
ORDER BY so.name, dbp.permission_name;


--повне резервування
BACKUP DATABASE gallery
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_FullDbBkup.bak' WITH INIT, NAME = 'gallery Full Db backup',
DESCRIPTION = 'gallery Full Database Backup'

RESTORE DATABASE gallery
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_FullDbBkup.bak'
WITH RECOVERY, REPLACE

--повне резервування з журналом
BACKUP DATABASE gallery
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_TlogBkup.bak'
WITH INIT, NAME = 'gallery Full Db backup',
DESCRIPTION = 'gallery Full Database Backup'

BACKUP LOG gallery
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_TlogBkup.bak'
WITH NOINIT, NAME = 'gallery Translog backup',
DESCRIPTION = 'gallery Transaction Log Backup', NOFORMAT

--резервування заключного фрагмента журналу 
BACKUP LOG gallery
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_TaillogBkup.bak'
WITH NORECOVERY

RESTORE DATABASE gallery
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_FullDbBkup.bak'
WITH RECOVERY

RESTORE LOG gallery
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_TlogBkup.bak'
WITH RECOVERY

RESTORE LOG gallery
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_TaillogBkup.bak' 
WITH NORECOVERY

BACKUP DATABASE gallery
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_DiffDbBkup.bak'
WITH INIT, DIFFERENTIAL, NAME = 'gallery Diff Db backup',
DESCRIPTION = 'gallery Differential Database Backup'

RESTORE DATABASE gallery
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\gallery_DiffDbBkup.bak'
WITH NORECOVERY